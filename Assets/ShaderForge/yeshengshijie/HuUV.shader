// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.32 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.32;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32830,y:32554,varname:node_3138,prsc:2|diff-2308-OUT,diffpow-980-OUT,spec-3750-OUT,gloss-5169-OUT,normal-7899-OUT,voffset-8216-OUT;n:type:ShaderForge.SFN_Tex2d,id:6989,x:32299,y:32435,varname:_Diffuse,prsc:2,tex:95c3e7866d78e08408dfb8fbbea0f144,ntxv:0,isnm:False|UVIN-325-UVOUT,TEX-7621-TEX;n:type:ShaderForge.SFN_Tex2d,id:4088,x:32142,y:32820,varname:_NormalMap,prsc:2,tex:b5ee3ded9db38ac4db7b7fed452a4f2a,ntxv:3,isnm:True|UVIN-325-UVOUT,TEX-2266-TEX;n:type:ShaderForge.SFN_Slider,id:980,x:32299,y:32582,ptovrint:False,ptlb:DiffusePower,ptin:_DiffusePower,varname:_DiffusePower,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:3750,x:32299,y:32675,ptovrint:False,ptlb:Specular,ptin:_Specular,varname:_Specular,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:5169,x:32299,y:32759,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:_Gloss,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Lerp,id:7899,x:32456,y:32838,varname:node_7899,prsc:2|A-434-OUT,B-5932-OUT,T-8264-OUT;n:type:ShaderForge.SFN_Vector3,id:434,x:32299,y:32838,varname:node_434,prsc:2,v1:0,v2:0,v3:1;n:type:ShaderForge.SFN_Slider,id:8264,x:32142,y:33089,ptovrint:False,ptlb:Normal,ptin:_Normal,varname:_Normal,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Sin,id:9595,x:32299,y:33210,varname:node_9595,prsc:2|IN-926-OUT;n:type:ShaderForge.SFN_Time,id:6068,x:31806,y:33210,varname:node_6068,prsc:2;n:type:ShaderForge.SFN_Multiply,id:8712,x:31985,y:33210,varname:node_8712,prsc:2|A-8099-OUT,B-6068-T;n:type:ShaderForge.SFN_ValueProperty,id:8099,x:31806,y:33135,ptovrint:False,ptlb:PinLv,ptin:_PinLv,varname:_PinLv,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:8216,x:32481,y:33210,varname:node_8216,prsc:2|A-9595-OUT,B-339-OUT,C-421-OUT;n:type:ShaderForge.SFN_ValueProperty,id:339,x:32299,y:33349,ptovrint:False,ptlb:QiangDu,ptin:_QiangDu,varname:_QiangDu,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Add,id:926,x:32142,y:33210,varname:node_926,prsc:2|A-8712-OUT,B-3372-OUT;n:type:ShaderForge.SFN_Multiply,id:3372,x:31985,y:33356,varname:node_3372,prsc:2|A-4086-OUT,B-3403-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3403,x:31806,y:33499,ptovrint:False,ptlb:BOChang,ptin:_BOChang,varname:_BOChang,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Vector3,id:421,x:32299,y:33582,varname:node_421,prsc:2,v1:0,v2:0,v3:1;n:type:ShaderForge.SFN_FragmentPosition,id:6397,x:31459,y:33391,varname:node_6397,prsc:2;n:type:ShaderForge.SFN_Add,id:4086,x:31806,y:33356,varname:node_4086,prsc:2|A-6397-X,B-6397-Z;n:type:ShaderForge.SFN_TexCoord,id:1805,x:31744,y:32413,varname:node_1805,prsc:2,uv:0;n:type:ShaderForge.SFN_Panner,id:325,x:31917,y:32560,varname:node_325,prsc:2,spu:0.01,spv:0.02|UVIN-1805-UVOUT;n:type:ShaderForge.SFN_Tex2dAsset,id:7621,x:32140,y:32382,ptovrint:False,ptlb: ,ptin:_,varname:node_7621,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:95c3e7866d78e08408dfb8fbbea0f144,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5226,x:32299,y:32315,varname:node_5226,prsc:2,tex:95c3e7866d78e08408dfb8fbbea0f144,ntxv:0,isnm:False|UVIN-6807-UVOUT,TEX-7621-TEX;n:type:ShaderForge.SFN_Panner,id:6807,x:31919,y:32267,varname:node_6807,prsc:2,spu:-0.003,spv:-0.008|UVIN-1805-UVOUT;n:type:ShaderForge.SFN_Multiply,id:2308,x:32456,y:32435,varname:node_2308,prsc:2|A-5226-RGB,B-6989-RGB;n:type:ShaderForge.SFN_Tex2dAsset,id:2266,x:31907,y:32903,ptovrint:False,ptlb:node_2266,ptin:_node_2266,varname:node_2266,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:b5ee3ded9db38ac4db7b7fed452a4f2a,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Multiply,id:5932,x:32299,y:32933,varname:node_5932,prsc:2|A-4088-RGB,B-7488-RGB;n:type:ShaderForge.SFN_Tex2d,id:7488,x:32142,y:32953,varname:node_7488,prsc:2,tex:b5ee3ded9db38ac4db7b7fed452a4f2a,ntxv:0,isnm:False|UVIN-6807-UVOUT,TEX-2266-TEX;proporder:980-3750-5169-8264-8099-339-3403-7621-2266;pass:END;sub:END;*/

Shader "Shader Forge/XiongUV" {
    Properties {
        _DiffusePower ("DiffusePower", Range(0, 1)) = 0
        _Specular ("Specular", Range(0, 1)) = 0
        _Gloss ("Gloss", Range(0, 1)) = 0
        _Normal ("Normal", Range(0, 1)) = 0
        _PinLv ("PinLv", Float ) = 1
        _QiangDu ("QiangDu", Float ) = 1
        _BOChang ("BOChang", Float ) = 1
        _ (" ", 2D) = "white" {}
        _node_2266 ("node_2266", 2D) = "bump" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float _DiffusePower;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float _Normal;
            uniform float _PinLv;
            uniform float _QiangDu;
            uniform float _BOChang;
            uniform sampler2D _; uniform float4 __ST;
            uniform sampler2D _node_2266; uniform float4 _node_2266_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float4 node_6068 = _Time + _TimeEditor;
                v.vertex.xyz += (sin(((_PinLv*node_6068.g)+((mul(unity_ObjectToWorld, v.vertex).r+mul(unity_ObjectToWorld, v.vertex).b)*_BOChang)))*_QiangDu*float3(0,0,1));
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 node_3419 = _Time + _TimeEditor;
                float2 node_325 = (i.uv0+node_3419.g*float2(0.01,0.02));
                float3 _NormalMap = UnpackNormal(tex2D(_node_2266,TRANSFORM_TEX(node_325, _node_2266)));
                float2 node_6807 = (i.uv0+node_3419.g*float2(-0.003,-0.008));
                float3 node_7488 = UnpackNormal(tex2D(_node_2266,TRANSFORM_TEX(node_6807, _node_2266)));
                float3 normalLocal = lerp(float3(0,0,1),(_NormalMap.rgb*node_7488.rgb),_Normal);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 specularColor = float3(_Specular,_Specular,_Specular);
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = pow(max( 0.0, NdotL), _DiffusePower) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float4 node_5226 = tex2D(_,TRANSFORM_TEX(node_6807, _));
                float4 _Diffuse = tex2D(_,TRANSFORM_TEX(node_325, _));
                float3 diffuseColor = (node_5226.rgb*_Diffuse.rgb);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform float _DiffusePower;
            uniform float _Specular;
            uniform float _Gloss;
            uniform float _Normal;
            uniform float _PinLv;
            uniform float _QiangDu;
            uniform float _BOChang;
            uniform sampler2D _; uniform float4 __ST;
            uniform sampler2D _node_2266; uniform float4 _node_2266_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float4 node_6068 = _Time + _TimeEditor;
                v.vertex.xyz += (sin(((_PinLv*node_6068.g)+((mul(unity_ObjectToWorld, v.vertex).r+mul(unity_ObjectToWorld, v.vertex).b)*_BOChang)))*_QiangDu*float3(0,0,1));
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 node_4174 = _Time + _TimeEditor;
                float2 node_325 = (i.uv0+node_4174.g*float2(0.01,0.02));
                float3 _NormalMap = UnpackNormal(tex2D(_node_2266,TRANSFORM_TEX(node_325, _node_2266)));
                float2 node_6807 = (i.uv0+node_4174.g*float2(-0.003,-0.008));
                float3 node_7488 = UnpackNormal(tex2D(_node_2266,TRANSFORM_TEX(node_6807, _node_2266)));
                float3 normalLocal = lerp(float3(0,0,1),(_NormalMap.rgb*node_7488.rgb),_Normal);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
///////// Gloss:
                float gloss = _Gloss;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float3 specularColor = float3(_Specular,_Specular,_Specular);
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularColor;
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = pow(max( 0.0, NdotL), _DiffusePower) * attenColor;
                float4 node_5226 = tex2D(_,TRANSFORM_TEX(node_6807, _));
                float4 _Diffuse = tex2D(_,TRANSFORM_TEX(node_325, _));
                float3 diffuseColor = (node_5226.rgb*_Diffuse.rgb);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
