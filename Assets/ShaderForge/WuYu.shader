// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.32 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.32;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:33422,y:32655,varname:node_3138,prsc:2|emission-8412-OUT,voffset-7164-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32978,y:32561,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_Tex2d,id:8647,x:32275,y:32729,ptovrint:False,ptlb:Text,ptin:_Text,varname:node_8647,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-8475-OUT;n:type:ShaderForge.SFN_Tex2d,id:3850,x:32635,y:33239,ptovrint:False,ptlb:Mask,ptin:_Mask,varname:_Mask,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ae0350d836ee5fd47bf62fabf90f6a15,ntxv:0,isnm:False;n:type:ShaderForge.SFN_NormalVector,id:8101,x:32558,y:32861,prsc:2,pt:False;n:type:ShaderForge.SFN_TexCoord,id:2192,x:31726,y:32684,varname:node_2192,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:5653,x:31936,y:32710,varname:node_5653,prsc:2|A-1858-OUT,B-2192-V;n:type:ShaderForge.SFN_Multiply,id:3763,x:31936,y:32572,varname:node_3763,prsc:2|A-4990-OUT,B-2192-U;n:type:ShaderForge.SFN_Slider,id:4990,x:31569,y:32513,ptovrint:False,ptlb:V_On,ptin:_V_On,varname:_V_On,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.9679237,max:2;n:type:ShaderForge.SFN_Slider,id:1858,x:31569,y:32616,ptovrint:False,ptlb:V_Down,ptin:_V_Down,varname:_V_Down,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5961319,max:2;n:type:ShaderForge.SFN_Add,id:9992,x:32639,y:32729,varname:node_9992,prsc:2|A-8647-RGB,B-3763-OUT,C-5653-OUT;n:type:ShaderForge.SFN_Multiply,id:5815,x:31936,y:32843,varname:node_5815,prsc:2|A-2192-U,B-7203-OUT;n:type:ShaderForge.SFN_Slider,id:7203,x:31559,y:32895,ptovrint:False,ptlb:U_Zoom,ptin:_U_Zoom,varname:_U_Zoom,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:10;n:type:ShaderForge.SFN_Add,id:6656,x:31936,y:32987,varname:node_6656,prsc:2|A-3528-OUT,B-9245-OUT;n:type:ShaderForge.SFN_Append,id:8475,x:32107,y:32762,varname:node_8475,prsc:2|A-5815-OUT,B-6656-OUT;n:type:ShaderForge.SFN_Multiply,id:3528,x:31761,y:32987,varname:node_3528,prsc:2|A-2192-V,B-649-OUT;n:type:ShaderForge.SFN_Slider,id:649,x:31371,y:32994,ptovrint:False,ptlb:V_Zoom,ptin:_V_Zoom,varname:_V_Zoom,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:10;n:type:ShaderForge.SFN_Multiply,id:9245,x:31761,y:33118,varname:node_9245,prsc:2|A-9193-OUT,B-6909-T;n:type:ShaderForge.SFN_Slider,id:9193,x:31371,y:33127,ptovrint:False,ptlb:V_Speed,ptin:_V_Speed,varname:_V_Speed,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:2;n:type:ShaderForge.SFN_Time,id:6909,x:31528,y:33197,varname:node_6909,prsc:2;n:type:ShaderForge.SFN_Multiply,id:7164,x:32805,y:32908,varname:node_7164,prsc:2|A-9992-OUT,B-8101-OUT,C-140-OUT,D-3850-RGB;n:type:ShaderForge.SFN_Slider,id:140,x:32289,y:33023,ptovrint:False,ptlb:Normal,ptin:_Normal,varname:_Normal,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_ComponentMask,id:9539,x:32805,y:32729,varname:node_9539,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-9992-OUT;n:type:ShaderForge.SFN_Multiply,id:8412,x:33187,y:32729,varname:node_8412,prsc:2|A-7241-RGB,B-5902-RGB;n:type:ShaderForge.SFN_Tex2d,id:5902,x:32978,y:32729,ptovrint:False,ptlb:Mask02,ptin:_Mask02,varname:node_5902,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-9539-OUT;proporder:7241-3850-4990-1858-7203-649-9193-140-8647-5902;pass:END;sub:END;*/

Shader "Shader Forge/WuYu" {
    Properties {
        _Color ("Color", Color) = (0.07843138,0.3921569,0.7843137,1)
        _Mask ("Mask", 2D) = "white" {}
        _V_On ("V_On", Range(0, 2)) = 0.9679237
        _V_Down ("V_Down", Range(0, 2)) = 0.5961319
        _U_Zoom ("U_Zoom", Range(0, 10)) = 0
        _V_Zoom ("V_Zoom", Range(0, 10)) = 0
        _V_Speed ("V_Speed", Range(0, 2)) = 0
        _Normal ("Normal", Range(0, 1)) = 0
        _Text ("Text", 2D) = "white" {}
        _Mask02 ("Mask02", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _Color;
            uniform sampler2D _Text; uniform float4 _Text_ST;
            uniform sampler2D _Mask; uniform float4 _Mask_ST;
            uniform float _V_On;
            uniform float _V_Down;
            uniform float _U_Zoom;
            uniform float _V_Zoom;
            uniform float _V_Speed;
            uniform float _Normal;
            uniform sampler2D _Mask02; uniform float4 _Mask02_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_6909 = _Time + _TimeEditor;
                float2 node_8475 = float2((o.uv0.r*_U_Zoom),((o.uv0.g*_V_Zoom)+(_V_Speed*node_6909.g)));
                float4 _Text_var = tex2Dlod(_Text,float4(TRANSFORM_TEX(node_8475, _Text),0.0,0));
                float3 node_9992 = (_Text_var.rgb+(_V_On*o.uv0.r)+(_V_Down*o.uv0.g));
                float4 _Mask_var = tex2Dlod(_Mask,float4(TRANSFORM_TEX(o.uv0, _Mask),0.0,0));
                v.vertex.xyz += (node_9992*v.normal*_Normal*_Mask_var.rgb);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_6909 = _Time + _TimeEditor;
                float2 node_8475 = float2((i.uv0.r*_U_Zoom),((i.uv0.g*_V_Zoom)+(_V_Speed*node_6909.g)));
                float4 _Text_var = tex2D(_Text,TRANSFORM_TEX(node_8475, _Text));
                float3 node_9992 = (_Text_var.rgb+(_V_On*i.uv0.r)+(_V_Down*i.uv0.g));
                float2 node_9539 = node_9992.rg;
                float4 _Mask02_var = tex2D(_Mask02,TRANSFORM_TEX(node_9539, _Mask02));
                float3 emissive = (_Color.rgb*_Mask02_var.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _Text; uniform float4 _Text_ST;
            uniform sampler2D _Mask; uniform float4 _Mask_ST;
            uniform float _V_On;
            uniform float _V_Down;
            uniform float _U_Zoom;
            uniform float _V_Zoom;
            uniform float _V_Speed;
            uniform float _Normal;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_6909 = _Time + _TimeEditor;
                float2 node_8475 = float2((o.uv0.r*_U_Zoom),((o.uv0.g*_V_Zoom)+(_V_Speed*node_6909.g)));
                float4 _Text_var = tex2Dlod(_Text,float4(TRANSFORM_TEX(node_8475, _Text),0.0,0));
                float3 node_9992 = (_Text_var.rgb+(_V_On*o.uv0.r)+(_V_Down*o.uv0.g));
                float4 _Mask_var = tex2Dlod(_Mask,float4(TRANSFORM_TEX(o.uv0, _Mask),0.0,0));
                v.vertex.xyz += (node_9992*v.normal*_Normal*_Mask_var.rgb);
                o.pos = UnityObjectToClipPos(v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
