// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.30 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.30;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:33536,y:32557,varname:node_3138,prsc:2|custl-9578-RGB,alpha-6645-OUT;n:type:ShaderForge.SFN_TexCoord,id:5165,x:32006,y:32798,varname:node_5165,prsc:2,uv:0;n:type:ShaderForge.SFN_RemapRange,id:8912,x:32175,y:32798,varname:node_8912,prsc:2,frmn:0,frmx:1,tomn:-3,tomx:3|IN-5165-UVOUT;n:type:ShaderForge.SFN_ComponentMask,id:9810,x:32331,y:32798,varname:node_9810,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-8912-OUT;n:type:ShaderForge.SFN_ArcTan2,id:6813,x:32507,y:32798,varname:node_6813,prsc:2,attp:2|A-9810-R,B-9810-G;n:type:ShaderForge.SFN_Length,id:1144,x:32507,y:32680,varname:node_1144,prsc:2|IN-8912-OUT;n:type:ShaderForge.SFN_Append,id:6407,x:32840,y:32798,varname:node_6407,prsc:2|A-1632-OUT,B-154-OUT;n:type:ShaderForge.SFN_Tex2d,id:9578,x:33033,y:32798,ptovrint:False,ptlb:Texture,ptin:_Texture,varname:node_9578,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-6407-OUT;n:type:ShaderForge.SFN_Add,id:1632,x:32681,y:32680,varname:node_1632,prsc:2|A-18-OUT,B-1144-OUT;n:type:ShaderForge.SFN_Multiply,id:18,x:32507,y:32554,varname:node_18,prsc:2|A-3346-OUT,B-5189-T;n:type:ShaderForge.SFN_Time,id:5189,x:32331,y:32680,varname:node_5189,prsc:2;n:type:ShaderForge.SFN_Slider,id:3346,x:32174,y:32581,ptovrint:False,ptlb:Slider,ptin:_Slider,varname:node_3346,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Add,id:154,x:32681,y:32798,varname:node_154,prsc:2|A-6813-OUT,B-5189-T;n:type:ShaderForge.SFN_Multiply,id:6645,x:33226,y:32798,varname:node_6645,prsc:2|A-9578-A,B-5600-A,C-1705-OUT;n:type:ShaderForge.SFN_VertexColor,id:5600,x:33033,y:32956,varname:node_5600,prsc:2;n:type:ShaderForge.SFN_Slider,id:1705,x:32876,y:32710,ptovrint:False,ptlb:1-0,ptin:_10,varname:node_1705,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;proporder:9578-3346-1705;pass:END;sub:END;*/

Shader "Shader Forge/Rotation" {
    Properties {
        _Texture ("Texture", 2D) = "white" {}
        _Slider ("Slider", Range(0, 1)) = 1
        _10 ("1-0", Range(0, 1)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _Slider;
            uniform float _10;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
                float4 node_5189 = _Time + _TimeEditor;
                float2 node_8912 = (i.uv0*6.0+-3.0);
                float2 node_9810 = node_8912.rg;
                float2 node_6407 = float2(((_Slider*node_5189.g)+length(node_8912)),(((atan2(node_9810.r,node_9810.g)/6.28318530718)+0.5)+node_5189.g));
                float4 _Texture_var = tex2D(_Texture,TRANSFORM_TEX(node_6407, _Texture));
                float3 finalColor = _Texture_var.rgb;
                return fixed4(finalColor,(_Texture_var.a*i.vertexColor.a*_10));
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
