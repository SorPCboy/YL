// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.30 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.30;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:33462,y:32802,varname:node_3138,prsc:2|normal-720-OUT,custl-1985-OUT,alpha-4825-OUT;n:type:ShaderForge.SFN_Cubemap,id:2833,x:32570,y:32732,ptovrint:False,ptlb:Cubemap_Text,ptin:_Cubemap_Text,varname:node_2833,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,cube:057648a8d6341984393624970950c1d2,pvfc:0;n:type:ShaderForge.SFN_ValueProperty,id:1140,x:32570,y:32881,ptovrint:False,ptlb:Cubemap_Value,ptin:_Cubemap_Value,varname:node_1140,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Tex2d,id:7989,x:32570,y:33237,ptovrint:False,ptlb:Diffuse,ptin:_Diffuse,varname:node_7989,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:1635,x:32945,y:33059,varname:node_1635,prsc:2|A-9282-OUT,B-7548-OUT,C-8090-OUT;n:type:ShaderForge.SFN_Multiply,id:7548,x:32788,y:33059,varname:node_7548,prsc:2|A-2833-RGB,B-1140-OUT,C-2451-OUT;n:type:ShaderForge.SFN_Transform,id:337,x:31895,y:32504,varname:node_337,prsc:2,tffrom:0,tfto:3|IN-2354-OUT;n:type:ShaderForge.SFN_NormalVector,id:2354,x:31727,y:32504,prsc:2,pt:False;n:type:ShaderForge.SFN_ComponentMask,id:8155,x:32058,y:32504,varname:node_8155,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-337-XYZ;n:type:ShaderForge.SFN_Tex2d,id:5728,x:32571,y:32504,ptovrint:False,ptlb:Glow,ptin:_Glow,varname:node_5728,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ab6b9ad8d1b763e4fb5d20b7d752957f,ntxv:0,isnm:False|UVIN-9750-OUT;n:type:ShaderForge.SFN_Multiply,id:74,x:32231,y:32504,varname:node_74,prsc:2|A-8155-OUT,B-4612-OUT;n:type:ShaderForge.SFN_Vector1,id:4612,x:32058,y:32651,varname:node_4612,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Add,id:9750,x:32409,y:32504,varname:node_9750,prsc:2|A-74-OUT,B-4612-OUT;n:type:ShaderForge.SFN_Multiply,id:9282,x:32785,y:32504,varname:node_9282,prsc:2|A-5728-RGB,B-6061-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6061,x:32571,y:32673,ptovrint:False,ptlb:Glow_Value,ptin:_Glow_Value,varname:node_6061,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Fresnel,id:2451,x:32570,y:32931,varname:node_2451,prsc:2|NRM-2354-OUT;n:type:ShaderForge.SFN_Tex2d,id:5754,x:32788,y:32881,ptovrint:False,ptlb:Normal,ptin:_Normal,varname:node_5754,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:679985fdf80de98489c78d5bedfe3786,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Multiply,id:8090,x:32788,y:33214,varname:node_8090,prsc:2|A-1986-RGB,B-7989-RGB;n:type:ShaderForge.SFN_Dot,id:5675,x:32250,y:33070,varname:node_5675,prsc:2,dt:4|A-2354-OUT,B-8043-OUT;n:type:ShaderForge.SFN_Vector1,id:3505,x:32250,y:33212,varname:node_3505,prsc:2,v1:0;n:type:ShaderForge.SFN_LightVector,id:8043,x:32083,y:33070,varname:node_8043,prsc:2;n:type:ShaderForge.SFN_Append,id:7193,x:32409,y:33070,varname:node_7193,prsc:2|A-5675-OUT,B-3505-OUT;n:type:ShaderForge.SFN_Tex2d,id:1986,x:32570,y:33070,ptovrint:False,ptlb:Dark,ptin:_Dark,varname:node_1986,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:37869bd1ef60bc1448ffa0fab5952e30,ntxv:0,isnm:False|UVIN-7193-OUT;n:type:ShaderForge.SFN_ComponentMask,id:3394,x:32945,y:32881,varname:node_3394,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-5754-RGB;n:type:ShaderForge.SFN_Multiply,id:38,x:33109,y:32881,varname:node_38,prsc:2|A-5684-OUT,B-3394-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5684,x:32945,y:32819,ptovrint:False,ptlb:Normal_Value,ptin:_Normal_Value,varname:node_5684,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.2;n:type:ShaderForge.SFN_Append,id:720,x:33273,y:32881,varname:node_720,prsc:2|A-38-OUT,B-5754-B;n:type:ShaderForge.SFN_Lerp,id:1985,x:33109,y:33059,varname:node_1985,prsc:2|A-1372-OUT,B-1635-OUT,T-4825-OUT;n:type:ShaderForge.SFN_Fresnel,id:8116,x:32788,y:33339,varname:node_8116,prsc:2|NRM-2354-OUT,EXP-9501-OUT;n:type:ShaderForge.SFN_Power,id:3697,x:32945,y:33214,varname:node_3697,prsc:2|VAL-8116-OUT,EXP-9501-OUT;n:type:ShaderForge.SFN_Vector1,id:9501,x:32570,y:33408,varname:node_9501,prsc:2,v1:2;n:type:ShaderForge.SFN_Slider,id:4825,x:32788,y:33512,ptovrint:False,ptlb:Lerp,ptin:_Lerp,varname:node_4825,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0.2,cur:0.2,max:1;n:type:ShaderForge.SFN_Multiply,id:1372,x:33109,y:33214,varname:node_1372,prsc:2|A-3697-OUT,B-8571-RGB,C-7989-RGB;n:type:ShaderForge.SFN_Color,id:8571,x:32945,y:33356,ptovrint:False,ptlb:Fresnel_Color,ptin:_Fresnel_Color,varname:node_8571,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;proporder:2833-1140-7989-5728-6061-5754-1986-5684-4825-8571;pass:END;sub:END;*/

Shader "Shader Forge/TuTuLe_shader_Stingray" {
    Properties {
        _Cubemap_Text ("Cubemap_Text", Cube) = "_Skybox" {}
        _Cubemap_Value ("Cubemap_Value", Float ) = 0.5
        _Diffuse ("Diffuse", 2D) = "white" {}
        _Glow ("Glow", 2D) = "white" {}
        _Glow_Value ("Glow_Value", Float ) = 0.5
        _Normal ("Normal", 2D) = "bump" {}
        _Dark ("Dark", 2D) = "white" {}
        _Normal_Value ("Normal_Value", Float ) = 0.2
        _Lerp ("Lerp", Range(0.2, 1)) = 0.2
        _Fresnel_Color ("Fresnel_Color", Color) = (0.5,0.5,0.5,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform samplerCUBE _Cubemap_Text;
            uniform float _Cubemap_Value;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform sampler2D _Glow; uniform float4 _Glow_ST;
            uniform float _Glow_Value;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform sampler2D _Dark; uniform float4 _Dark_ST;
            uniform float _Normal_Value;
            uniform float _Lerp;
            uniform float4 _Fresnel_Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 normalLocal = float3((_Normal_Value*_Normal_var.rgb.rg),_Normal_var.b);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
                float node_9501 = 2.0;
                float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
                float node_4612 = 0.5;
                float2 node_9750 = ((mul( UNITY_MATRIX_V, float4(i.normalDir,0) ).xyz.rgb.rg*node_4612)+node_4612);
                float4 _Glow_var = tex2D(_Glow,TRANSFORM_TEX(node_9750, _Glow));
                float2 node_7193 = float2(0.5*dot(i.normalDir,lightDirection)+0.5,0.0);
                float4 _Dark_var = tex2D(_Dark,TRANSFORM_TEX(node_7193, _Dark));
                float3 finalColor = lerp((pow(pow(1.0-max(0,dot(i.normalDir, viewDirection)),node_9501),node_9501)*_Fresnel_Color.rgb*_Diffuse_var.rgb),((_Glow_var.rgb*_Glow_Value)+(texCUBE(_Cubemap_Text,viewReflectDirection).rgb*_Cubemap_Value*(1.0-max(0,dot(i.normalDir, viewDirection))))+(_Dark_var.rgb*_Diffuse_var.rgb)),_Lerp);
                return fixed4(finalColor,_Lerp);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform samplerCUBE _Cubemap_Text;
            uniform float _Cubemap_Value;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform sampler2D _Glow; uniform float4 _Glow_ST;
            uniform float _Glow_Value;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform sampler2D _Dark; uniform float4 _Dark_ST;
            uniform float _Normal_Value;
            uniform float _Lerp;
            uniform float4 _Fresnel_Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 normalLocal = float3((_Normal_Value*_Normal_var.rgb.rg),_Normal_var.b);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:
                float node_9501 = 2.0;
                float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
                float node_4612 = 0.5;
                float2 node_9750 = ((mul( UNITY_MATRIX_V, float4(i.normalDir,0) ).xyz.rgb.rg*node_4612)+node_4612);
                float4 _Glow_var = tex2D(_Glow,TRANSFORM_TEX(node_9750, _Glow));
                float2 node_7193 = float2(0.5*dot(i.normalDir,lightDirection)+0.5,0.0);
                float4 _Dark_var = tex2D(_Dark,TRANSFORM_TEX(node_7193, _Dark));
                float3 finalColor = lerp((pow(pow(1.0-max(0,dot(i.normalDir, viewDirection)),node_9501),node_9501)*_Fresnel_Color.rgb*_Diffuse_var.rgb),((_Glow_var.rgb*_Glow_Value)+(texCUBE(_Cubemap_Text,viewReflectDirection).rgb*_Cubemap_Value*(1.0-max(0,dot(i.normalDir, viewDirection))))+(_Dark_var.rgb*_Diffuse_var.rgb)),_Lerp);
                return fixed4(finalColor * _Lerp,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
