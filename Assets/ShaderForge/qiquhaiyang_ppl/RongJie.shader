// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.30 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.30;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32747,y:32538,varname:node_3138,prsc:2|custl-6637-OUT,alpha-3939-OUT,clip-2900-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32241,y:32777,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_Multiply,id:1352,x:32403,y:32777,varname:node_1352,prsc:2|A-7241-RGB,B-8053-RGB;n:type:ShaderForge.SFN_Tex2d,id:8053,x:32241,y:32935,ptovrint:False,ptlb:Texture,ptin:_Texture,varname:node_8053,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:7e2e89cfdce4e8d4a946ede598b56704,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:1190,x:31746,y:33152,ptovrint:False,ptlb:Diss_Text,ptin:_Diss_Text,varname:node_1190,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:06e444f7dd51d0b4e8319e51d265e01a,ntxv:0,isnm:False;n:type:ShaderForge.SFN_VertexColor,id:3021,x:32403,y:32952,varname:node_3021,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3939,x:32567,y:32952,varname:node_3939,prsc:2|A-8053-A,B-3021-A;n:type:ShaderForge.SFN_If,id:5827,x:32074,y:33087,varname:node_5827,prsc:2|A-1053-OUT,B-1190-R,GT-8638-OUT,EQ-8638-OUT,LT-1758-OUT;n:type:ShaderForge.SFN_If,id:7396,x:32074,y:33216,varname:node_7396,prsc:2|A-1053-OUT,B-347-OUT,GT-8638-OUT,EQ-8638-OUT,LT-1758-OUT;n:type:ShaderForge.SFN_Multiply,id:347,x:31903,y:33254,varname:node_347,prsc:2|A-1190-R,B-779-OUT;n:type:ShaderForge.SFN_Vector1,id:8638,x:31903,y:33143,varname:node_8638,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:1758,x:31903,y:33203,varname:node_1758,prsc:2,v1:0;n:type:ShaderForge.SFN_Slider,id:1053,x:31746,y:33061,ptovrint:False,ptlb:A,ptin:_A,varname:node_1053,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Subtract,id:8266,x:32241,y:33087,varname:node_8266,prsc:2|A-5827-OUT,B-7396-OUT;n:type:ShaderForge.SFN_Multiply,id:2699,x:32403,y:33087,varname:node_2699,prsc:2|A-8266-OUT,B-9509-RGB;n:type:ShaderForge.SFN_Color,id:9509,x:32241,y:33238,ptovrint:False,ptlb:Dis_Color,ptin:_Dis_Color,varname:node_9509,prsc:2,glob:False,taghide:False,taghdr:True,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Add,id:2900,x:32560,y:33087,varname:node_2900,prsc:2|A-5827-OUT,B-8266-OUT;n:type:ShaderForge.SFN_Add,id:6637,x:32567,y:32777,varname:node_6637,prsc:2|A-1352-OUT,B-2699-OUT;n:type:ShaderForge.SFN_ValueProperty,id:779,x:31746,y:33320,ptovrint:False,ptlb:Diss_Value,ptin:_Diss_Value,varname:node_779,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_TexCoord,id:2820,x:33083,y:33983,varname:node_2820,prsc:2,uv:0;n:type:ShaderForge.SFN_TexCoord,id:2747,x:32706,y:33645,varname:node_2747,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:9148,x:32549,y:33803,ptovrint:False,ptlb:Inside,ptin:_Inside,varname:node_2568,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:-0.3295126,max:0;n:type:ShaderForge.SFN_Lerp,id:7325,x:32904,y:33645,varname:node_7325,prsc:2|A-2747-UVOUT,B-6124-OUT,T-9148-OUT;n:type:ShaderForge.SFN_Tex2d,id:8715,x:33442,y:33649,cmnt:上波浪,varname:node_1668,prsc:2,ntxv:0,isnm:False|TEX-7551-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:4372,x:32170,y:33878,ptovrint:False,ptlb:FlowMap,ptin:_FlowMap,varname:node_4877,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5219,x:32900,y:33809,varname:node_6325,prsc:2,ntxv:0,isnm:False|UVIN-4805-UVOUT,TEX-4372-TEX;n:type:ShaderForge.SFN_Panner,id:4805,x:32549,y:33885,varname:node_4805,prsc:2,spu:0,spv:0.25|UVIN-5135-UVOUT;n:type:ShaderForge.SFN_Multiply,id:4535,x:33089,y:33809,varname:node_4535,prsc:2|A-5219-R,B-91-OUT,C-7638-OUT;n:type:ShaderForge.SFN_Slider,id:91,x:32743,y:34103,ptovrint:False,ptlb:WaveIntensity,ptin:_WaveIntensity,varname:node_30,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:-1,max:1;n:type:ShaderForge.SFN_Add,id:2147,x:33504,y:34031,varname:node_2147,prsc:2|A-8315-UVOUT,B-4535-OUT,C-9273-OUT;n:type:ShaderForge.SFN_Slider,id:188,x:32153,y:34384,ptovrint:False,ptlb:Value,ptin:_Value,varname:node_4337,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Append,id:9273,x:33235,y:34276,varname:node_9273,prsc:2|A-1624-OUT,B-3287-OUT;n:type:ShaderForge.SFN_Vector1,id:1624,x:33030,y:34318,varname:node_1624,prsc:2,v1:0;n:type:ShaderForge.SFN_Add,id:6523,x:32904,y:34377,varname:node_6523,prsc:2|A-188-OUT,B-3319-OUT;n:type:ShaderForge.SFN_Vector1,id:3319,x:32551,y:34405,varname:node_3319,prsc:2,v1:0.47;n:type:ShaderForge.SFN_OneMinus,id:3287,x:33030,y:34377,varname:node_3287,prsc:2|IN-6523-OUT;n:type:ShaderForge.SFN_ConstantLerp,id:7638,x:32900,y:33943,varname:node_7638,prsc:2,a:0.5,b:1|IN-188-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:7551,x:33261,y:33809,ptovrint:False,ptlb:WaveTex,ptin:_WaveTex,varname:node_6349,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:9056,x:33442,y:33809,cmnt:下波浪,varname:node_7456,prsc:2,ntxv:0,isnm:False|TEX-7551-TEX;n:type:ShaderForge.SFN_Add,id:5624,x:33535,y:34172,varname:node_5624,prsc:2|A-2820-UVOUT,B-9992-OUT,C-9273-OUT,D-6016-OUT;n:type:ShaderForge.SFN_Append,id:6016,x:33083,y:34538,varname:node_6016,prsc:2|A-2987-OUT,B-34-OUT;n:type:ShaderForge.SFN_Tex2d,id:5043,x:32900,y:34172,varname:node_3777,prsc:2,ntxv:0,isnm:False|UVIN-9386-UVOUT,TEX-4372-TEX;n:type:ShaderForge.SFN_Panner,id:9386,x:32743,y:34172,varname:node_9386,prsc:2,spu:0,spv:0.3|UVIN-7698-UVOUT;n:type:ShaderForge.SFN_Multiply,id:9992,x:33083,y:34147,varname:node_9992,prsc:2|A-5043-R,B-91-OUT,C-9795-OUT;n:type:ShaderForge.SFN_Vector1,id:9795,x:32551,y:34321,varname:node_9795,prsc:2,v1:1.25;n:type:ShaderForge.SFN_Vector1,id:2987,x:32904,y:34538,varname:node_2987,prsc:2,v1:0;n:type:ShaderForge.SFN_ConstantLerp,id:34,x:32904,y:34596,varname:node_34,prsc:2,a:0.1,b:0|IN-188-OUT;n:type:ShaderForge.SFN_TexCoord,id:5135,x:32392,y:33885,varname:node_5135,prsc:2,uv:0;n:type:ShaderForge.SFN_TexCoord,id:7698,x:32551,y:34172,varname:node_7698,prsc:2,uv:0;n:type:ShaderForge.SFN_TexCoord,id:8315,x:33089,y:33649,varname:node_8315,prsc:2,uv:0;n:type:ShaderForge.SFN_Vector2,id:6124,x:32706,y:33873,varname:node_6124,prsc:2,v1:0.5,v2:0.5;proporder:7241-8053-1190-1053-9509-779;pass:END;sub:END;*/

Shader "Shader Forge/RongJie" {
    Properties {
        _Color ("Color", Color) = (0.07843138,0.3921569,0.7843137,1)
        _Texture ("Texture", 2D) = "white" {}
        _Diss_Text ("Diss_Text", 2D) = "white" {}
        _A ("A", Range(0, 1)) = 0
        [HDR]_Dis_Color ("Dis_Color", Color) = (0.5,0.5,0.5,1)
        _Diss_Value ("Diss_Value", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform sampler2D _Diss_Text; uniform float4 _Diss_Text_ST;
            uniform float _A;
            uniform float4 _Dis_Color;
            uniform float _Diss_Value;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 _Diss_Text_var = tex2D(_Diss_Text,TRANSFORM_TEX(i.uv0, _Diss_Text));
                float node_5827_if_leA = step(_A,_Diss_Text_var.r);
                float node_5827_if_leB = step(_Diss_Text_var.r,_A);
                float node_1758 = 0.0;
                float node_8638 = 1.0;
                float node_5827 = lerp((node_5827_if_leA*node_1758)+(node_5827_if_leB*node_8638),node_8638,node_5827_if_leA*node_5827_if_leB);
                float node_7396_if_leA = step(_A,(_Diss_Text_var.r*_Diss_Value));
                float node_7396_if_leB = step((_Diss_Text_var.r*_Diss_Value),_A);
                float node_8266 = (node_5827-lerp((node_7396_if_leA*node_1758)+(node_7396_if_leB*node_8638),node_8638,node_7396_if_leA*node_7396_if_leB));
                clip((node_5827+node_8266) - 0.5);
////// Lighting:
                float4 _Texture_var = tex2D(_Texture,TRANSFORM_TEX(i.uv0, _Texture));
                float3 node_2699 = (node_8266*_Dis_Color.rgb);
                float3 finalColor = ((_Color.rgb*_Texture_var.rgb)+node_2699);
                return fixed4(finalColor,(_Texture_var.a*i.vertexColor.a));
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _Diss_Text; uniform float4 _Diss_Text_ST;
            uniform float _A;
            uniform float _Diss_Value;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 _Diss_Text_var = tex2D(_Diss_Text,TRANSFORM_TEX(i.uv0, _Diss_Text));
                float node_5827_if_leA = step(_A,_Diss_Text_var.r);
                float node_5827_if_leB = step(_Diss_Text_var.r,_A);
                float node_1758 = 0.0;
                float node_8638 = 1.0;
                float node_5827 = lerp((node_5827_if_leA*node_1758)+(node_5827_if_leB*node_8638),node_8638,node_5827_if_leA*node_5827_if_leB);
                float node_7396_if_leA = step(_A,(_Diss_Text_var.r*_Diss_Value));
                float node_7396_if_leB = step((_Diss_Text_var.r*_Diss_Value),_A);
                float node_8266 = (node_5827-lerp((node_7396_if_leA*node_1758)+(node_7396_if_leB*node_8638),node_8638,node_7396_if_leA*node_7396_if_leB));
                clip((node_5827+node_8266) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
