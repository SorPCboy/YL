// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.30 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.30;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32904,y:32812,varname:node_3138,prsc:2|custl-6355-OUT,alpha-9514-A;n:type:ShaderForge.SFN_TexCoord,id:1663,x:31832,y:33037,varname:node_1663,prsc:2,uv:0;n:type:ShaderForge.SFN_Panner,id:5321,x:32016,y:33037,varname:node_5321,prsc:2,spu:0,spv:-0.8|UVIN-1663-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:5911,x:32199,y:33037,ptovrint:False,ptlb:node_5911,ptin:_node_5911,varname:node_5911,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-5321-UVOUT;n:type:ShaderForge.SFN_Multiply,id:6355,x:32569,y:33050,varname:node_6355,prsc:2|A-9144-OUT,B-9514-RGB;n:type:ShaderForge.SFN_VertexColor,id:9514,x:32383,y:33227,varname:node_9514,prsc:2;n:type:ShaderForge.SFN_Multiply,id:9144,x:32405,y:33037,varname:node_9144,prsc:2|A-5911-RGB,B-7422-RGB;n:type:ShaderForge.SFN_Color,id:7422,x:32199,y:33217,ptovrint:False,ptlb:node_7422,ptin:_node_7422,varname:node_7422,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;proporder:5911-7422;pass:END;sub:END;*/

Shader "Shader Forge/PenShui" {
    Properties {
        _node_5911 ("node_5911", 2D) = "white" {}
        _node_7422 ("node_7422", Color) = (0.5,0.5,0.5,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_5911; uniform float4 _node_5911_ST;
            uniform float4 _node_7422;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
                float4 node_6276 = _Time + _TimeEditor;
                float2 node_5321 = (i.uv0+node_6276.g*float2(0,-0.8));
                float4 _node_5911_var = tex2D(_node_5911,TRANSFORM_TEX(node_5321, _node_5911));
                float3 finalColor = ((_node_5911_var.rgb*_node_7422.rgb)*i.vertexColor.rgb);
                return fixed4(finalColor,i.vertexColor.a);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
